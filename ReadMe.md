# Getting Started with TypeScript
Starter TypeScript Project.

# Path to Awesomneness
- Clone this repo
- Install dependencies: `npm install`
- Run the TypeScript compiler: `tsc --watch`
- Run the app from the dist directory: `node app.js`
